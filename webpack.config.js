const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { merge } = require('webpack-merge');
const CssoWebpackPlugin = require('csso-webpack-plugin').default;
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CreateContentHashFilePlugin } = require('../../webpack/plugins/create-content-hash-file/index');

module.exports = merge(require('../../builder/webpack.config.js'), {
  entry:  './src/index.js',
  output: {
    filename: '[name].[contenthash].js',
    path:     path.resolve(__dirname, 'dist'),
  },
  plugins: [
    new CleanWebpackPlugin({ cleanStaleWebpackAssets: false }),
    new CreateContentHashFilePlugin(),
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename:      '[name].[contenthash].css',
      chunkFilename: '[id].css',
    }),
    new CssoWebpackPlugin({
      comments: false,
    }),
  ],
  resolve: {
    alias: {
      '~static': path.resolve('../../'),
    }
  },
  module: {
    rules: [
      {
        test:   /\.vue$/,
        loader: 'vue-loader',
      },
      {
        test: /\.png$/,
        use:  'file-loader',
      },
      {
        test:    /\.html$/,
        exclude: /node_modules/,
        use:     { loader: 'html-loader' },
      },
      {
        test:    /\.scss$/,
        exclude: /node_modules/,
        use:     [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader', 'prettier-loader'],
      },
      {
        enforce: 'pre',
        test:    /\.(js|.vue)$/,
        exclude: [/node_modules/],
        loader:  'eslint-loader',
      }
    ],
  },
});
