import { shallowMount } from "@vue/test-utils";
import City from "../../src/components/City.vue";

describe('testing City', () => {
  it('should have m-loading class if such prop passed', () => {
    expect.hasAssertions();

    const wrapper = shallowMount(City, {
      propsData: {
        name:      'Test city',
        count:     1,
        isLoading: true,
      }
    });

    expect(wrapper.classes()).toContain('m-loading');
  });

  it('should not have m-loading class if such props is not passed', () => {
    expect.hasAssertions();

    const wrapper = shallowMount(City, {
      propsData: {
        name:      'Test city',
        count:     1,
        isLoading: false,
      }
    });

    expect(wrapper.classes()).not.toContain('m-loading');
  });

  it('should contain passed name and count', () => {
    expect.hasAssertions();

    const TEST_NAME = 'TEST_CITY';
    const TEST_COUNT = 666;
    const wrapper = shallowMount(City, {
      propsData: {
        name:      TEST_NAME,
        count:     TEST_COUNT,
        isLoading: true,
      }
    });

    expect(wrapper.text()).toContain(TEST_NAME);
    expect(wrapper.text()).toContain(TEST_COUNT.toString())
  });
});