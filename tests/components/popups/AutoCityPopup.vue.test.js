// eslint-disable-next-line max-classes-per-file
import { shallowMount } from '@vue/test-utils';
import AutoCityPopup from '../../../src/components/popups/AutoCityPopup.vue';
import constants from '../../../src/constants.js';

const getOpener = () => {
  return document.createElement('span');
};

// eslint-disable-next-line jest/require-top-level-describe
beforeEach(() => {
  // eslint-disable-next-line jest/prefer-spy-on
  window.matchMedia = jest.fn().mockReturnValue({
    matches: true,
  });
  // eslint-disable-next-line jest/prefer-spy-on
  window.$ = jest.fn().mockImplementation(() => {
    return {
      when: jest.fn().mockImplementation(() => {
        jest.fn();
      }),
      on: jest.fn(),
    };
  });

  window.Sunlight = {
    // NewSlideFromBottomPopup: SlideFromBottomPopup,
  };
});

describe('testing methods', () => {
  describe('testing getClassName', () => {
    it('should return cities-auto__popup__mobile if isMobileVersion', () => {
      expect.hasAssertions();

      jest.spyOn(AutoCityPopup.methods, 'isMobileVersion').mockReturnValue(true);

      expect(AutoCityPopup.methods.getClassName()).toStrictEqual('cities-auto__popup__mobile');
    });

    it('should return empty string if not isMobileVersion', () => {
      expect.hasAssertions();

      jest.spyOn(AutoCityPopup.methods, 'isMobileVersion').mockReturnValue(false);

      expect(AutoCityPopup.methods.getClassName()).toStrictEqual('');
    });
  });

  describe('testing close', () => {
    it('should emit input with false', () => {
      expect.hasAssertions();

      const wrapper = shallowMount(AutoCityPopup, {
        propsData: {
          opener:    getOpener(),
          value:     false,
          params:    {},
          isLoading: false,
        },
      });

      wrapper.vm.close();

      expect(wrapper.emitted('input')).toHaveLength(1);
      expect(wrapper.emitted('input')[0]).toStrictEqual([false]);
    });
  });

  describe('testing setUnsettled', () => {
    it('should emit set-unsettled and input with false', () => {
      expect.hasAssertions();

      const wrapper = shallowMount(AutoCityPopup, {
        propsData: {
          opener:    getOpener(),
          value:     false,
          params:    {},
          isLoading: false,
        },
      });

      wrapper.vm.setUnsettled();

      expect(wrapper.emitted('input')).toHaveLength(1);
      expect(wrapper.emitted('input')[0]).toStrictEqual([false]);

      expect(wrapper.emitted('set-unsettled')).toHaveLength(1);
    });
  });

  describe('testing applyAuto', () => {
    it('should emit set-city with expected params safe param and result', () => {
      expect.hasAssertions();

      const TEST_RESULT = {
        fiasId: 'test-fias-id',
        name:   'test-name',
      };
      const wrapper = shallowMount(AutoCityPopup, {
        propsData: {
          opener:    getOpener(),
          value:     false,
          params:    {},
          isLoading: false,
        },
      });

      wrapper.vm.res = TEST_RESULT;

      wrapper.vm.applyAuto(true);
      wrapper.vm.applyAuto(false);

      expect(wrapper.emitted('set-city')).toHaveLength(2);
      expect(wrapper.emitted('set-city')[0]).toStrictEqual([{ ...TEST_RESULT, safe: true }]);
      expect(wrapper.emitted('set-city')[1]).toStrictEqual([{ ...TEST_RESULT, safe: false }]);
    });
  });

  describe('testing open', () => {
    it('should create popup if one is not initialized', () => {
      expect.hasAssertions();

      window.Sunlight = {
        loadFoldedCSS:           jest.fn(),
        NewSlideFromBottomPopup: jest.fn(),
        popupLoader:             {
          loadCSS: jest.fn(),
        },
      };

      // eslint-disable-next-line jest/prefer-spy-on
      window.$.when = jest.fn().mockReturnValue({
        done: jest.fn().mockReturnValue({
          fail: jest.fn(),
        }),
      });

      const wrapper = shallowMount(AutoCityPopup, {
        propsData: {
          opener:    getOpener(),
          value:     false,
          params:    {},
          isLoading: false,
        },
      });

      wrapper.vm.open();

      expect(window.Sunlight.NewSlideFromBottomPopup).toHaveBeenCalledTimes(1);
    });

    it('should not create popup if one already initialized', () => {
      expect.hasAssertions();

      window.Sunlight = {
        loadFoldedCSS:           jest.fn(),
        NewSlideFromBottomPopup: jest.fn(),
        popupLoader:             {
          loadCSS: jest.fn(),
        },
      };

      // eslint-disable-next-line jest/prefer-spy-on
      window.$.when = jest.fn().mockReturnValue({
        done: jest.fn().mockReturnValue({
          fail: jest.fn(),
        }),
      });

      const wrapper = shallowMount(AutoCityPopup, {
        propsData: {
          opener:    getOpener(),
          value:     false,
          params:    {},
          isLoading: false,
        },
      });

      wrapper.vm.popup = new window.Sunlight.NewSlideFromBottomPopup();

      wrapper.vm.open();

      expect(window.Sunlight.NewSlideFromBottomPopup).toHaveBeenCalledTimes(1);
    });

    it('should call detectCity, open popup, set cookie, call applyAuto', async () => {
      expect.hasAssertions();

      document.cookie = '';

      const TEST_REGION_NAME = 'test-name';
      const openPopupMock = jest.fn();
      const detectCityMock = jest.fn().mockResolvedValue({
        name:    TEST_REGION_NAME,
        default: false,
      });

      window.Sunlight = {
        loadFoldedCSS:           jest.fn(),
        NewSlideFromBottomPopup: jest.fn().mockImplementation(() => {
          return {
            open: openPopupMock,
          };
        }),
        popupLoader: {
          loadCSS: jest.fn(),
        },
        modules: {
          regions: {
            detectCity: detectCityMock,
          },
        },
      };

      window.SL_SETTINGS = {};

      window.Cookies = {
        set: jest.fn(),
      };

      class FakePromise {
        done(fn) {
          fn();
          return this;
        }

        fail(fn) {
          return this;
        }
      }

      // eslint-disable-next-line jest/prefer-spy-on
      window.$.when = jest.fn().mockReturnValue(new FakePromise());

      jest.spyOn(AutoCityPopup.methods, 'applyAuto');

      const opener = getOpener();
      const wrapper = shallowMount(AutoCityPopup, {
        propsData: {
          opener,
          value:     false,
          params:    {},
          isLoading: false,
        },
      });
      const flushPromises = () => {
        return new Promise(setImmediate);
      }; // awaiting next tick

      wrapper.vm.open();
      await flushPromises();

      expect(detectCityMock).toHaveBeenCalledWith({ save: false });
      expect(openPopupMock).toHaveBeenCalledWith();
      expect(wrapper.vm.opener.innerHTML).toStrictEqual(TEST_REGION_NAME);
      expect(AutoCityPopup.methods.applyAuto).toHaveBeenCalledWith(true, false);

      expect(window.Cookies.set).toHaveBeenCalledWith(constants.cityPopupIsAlreadyShownKey, 1, {
        domain:  undefined,
        expires: 7,
      });
    });

    it('should not call applyAuto if detected city is default', async () => {
      expect.hasAssertions();

      const detectCityMock = jest.fn().mockResolvedValue({
        default: true,
      });

      window.Sunlight = {
        loadFoldedCSS:           jest.fn(),
        NewSlideFromBottomPopup: jest.fn().mockImplementation(() => {
          return {
            open: jest.fn(),
          };
        }),
        popupLoader: {
          loadCSS: jest.fn(),
        },
        modules: {
          regions: {
            detectCity: detectCityMock,
          },
        },
      };

      window.SL_SETTINGS = {};

      window.Cookies = {
        set: jest.fn(),
      };

      class FakePromise {
        done(fn) {
          fn();
          return this;
        }

        fail(fn) {
          return this;
        }
      }

      // eslint-disable-next-line jest/prefer-spy-on
      window.$.when = jest.fn().mockReturnValue(new FakePromise());

      jest.spyOn(AutoCityPopup.methods, 'applyAuto');

      const wrapper = shallowMount(AutoCityPopup, {
        propsData: {
          opener:    getOpener(),
          value:     false,
          params:    {},
          isLoading: false,
        },
      });
      const flushPromises = () => {
        return new Promise(setImmediate);
      }; // awaiting next tick

      wrapper.vm.open();
      await flushPromises();

      expect(AutoCityPopup.methods.applyAuto).not.toHaveBeenCalled();
    });

    it('should set cookies expired within 1 day if loading popup somehow failed', async () => {
      expect.hasAssertions();

      window.Sunlight = {
        loadFoldedCSS: jest.fn(),
        popupLoader:   {
          loadCSS: jest.fn(),
        },
      };

      window.SL_SETTINGS = {};

      window.Cookies = {
        set: jest.fn(),
      };

      class FakePromise {
        done(fn) {
          return this;
        }

        fail(fn) {
          fn();
          return this;
        }
      }

      // eslint-disable-next-line jest/prefer-spy-on
      window.$.when = jest.fn().mockReturnValue(new FakePromise());

      jest.spyOn(AutoCityPopup.methods, 'applyAuto');

      const wrapper = shallowMount(AutoCityPopup, {
        propsData: {
          opener:    getOpener(),
          value:     false,
          params:    {},
          isLoading: false,
        },
      });
      const flushPromises = () => {
        return new Promise(setImmediate);
      }; // awaiting next tick

      wrapper.vm.popup = {};
      wrapper.vm.open();
      await flushPromises();

      expect(window.Cookies.set).toHaveBeenCalledWith(constants.cityPopupIsAlreadyShownKey, 1, {
        domain:  undefined,
        expires: 1,
      });
    });
  });
});
