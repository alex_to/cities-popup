import userEvent from '@testing-library/user-event';
import { enableAutoDestroy, shallowMount } from '@vue/test-utils';
import CitiesPopup from '../../../src/components/popups/CitiesPopup.vue';

enableAutoDestroy(afterEach);

describe('testing methods', () => {
  describe('testing onAllCitiesAreScrolling', () => {
    it('should set allCitiesAreScrolling to true if scrolltop is not 0', () => {
      expect.hasAssertions();

      const e = {
        target: {
          scrollTop: 1,
        },
      };

      jest.spyOn(CitiesPopup, 'created').mockReturnValue({});

      const wrapper = shallowMount(CitiesPopup, {
        propsData: {
          params:    {},
          value:     false,
          isLoading: true,
          cityName:  '',
        },
      });

      wrapper.vm.onAllCitiesAreScrolling(e);

      expect(wrapper.vm.allCitiesAreScrolling).toBe(true);
    });

    it('should set allCitiesAreScrolling to false if scrolltop is 0', async () => {
      expect.hasAssertions();

      const e = {
        target: {
          scrollTop: 0,
        },
      };

      jest.spyOn(CitiesPopup, 'created').mockReturnValue({});

      const wrapper = shallowMount(CitiesPopup, {
        propsData: {
          params:    {},
          value:     false,
          isLoading: true,
          cityName:  '',
        },
      });

      await wrapper.setData({
        allCitiesAreScrolling: true,
      });

      wrapper.vm.onAllCitiesAreScrolling(e);

      expect(wrapper.vm.allCitiesAreScrolling).toBe(false);
    });
  });

  describe('testing toggleShowAllCities', () => {
    it('should set allCitiesAreShowed to false if current value is true', async () => {
      expect.hasAssertions();

      jest.spyOn(CitiesPopup, 'created').mockReturnValue({});

      const wrapper = shallowMount(CitiesPopup, {
        propsData: {
          params:    {},
          value:     false,
          isLoading: true,
          cityName:  '',
        },
      });

      await wrapper.setData({
        allCitiesAreShowed: true,
      });

      wrapper.vm.toggleShowAllCities();

      expect(wrapper.vm.allCitiesAreShowed).toBe(false);
    });

    it('should set allCitiesAreShowed to true if current value is false', async () => {
      expect.hasAssertions();

      jest.spyOn(CitiesPopup, 'created').mockReturnValue({});

      const wrapper = shallowMount(CitiesPopup, {
        propsData: {
          params:    {},
          value:     false,
          isLoading: true,
          cityName:  '',
        },
      });

      await wrapper.setData({
        allCitiesAreShowed: false,
      });

      wrapper.vm.toggleShowAllCities();

      expect(wrapper.vm.allCitiesAreShowed).toBe(true);
    });
  });

  describe('testing setCity', () => {
    it('should emit set-city', () => {
      expect.hasAssertions();

      jest.spyOn(CitiesPopup, 'created').mockReturnValue({});

      const wrapper = shallowMount(CitiesPopup, {
        propsData: {
          params:    {},
          value:     false,
          isLoading: true,
          cityName:  '',
        },
      });

      const TEST_CITY = {
        fiasId: 'fias-id',
      };

      wrapper.vm.setCity(TEST_CITY);

      const emitted = wrapper.emitted('set-city');

      expect(emitted).toHaveLength(1);
      expect(emitted[0]).toStrictEqual([TEST_CITY]);
    });
  });

  describe('testing autoCheck', () => {
    it('should not call Sunlight.modules.regions if isLoading', async () => {
      expect.hasAssertions();

      const detectCityMock = jest.fn();

      jest.spyOn(CitiesPopup, 'created').mockReturnValue({});

      window.Sunlight = {
        modules: {
          regions: {
            detectCity: detectCityMock,
          },
        },
      };

      const wrapper = shallowMount(CitiesPopup, {
        propsData: {
          params:    {},
          value:     false,
          isLoading: true,
          cityName:  '',
        },
      });

      await wrapper.vm.autoCheck();

      expect(detectCityMock).not.toHaveBeenCalled();
    });

    it('should not call Sunlight.modules.regions if autoCheckLoading', async () => {
      expect.hasAssertions();

      const detectCityMock = jest.fn();

      jest.spyOn(CitiesPopup, 'created').mockReturnValue({});

      window.Sunlight = {
        modules: {
          regions: {
            detectCity: detectCityMock,
          },
        },
      };

      const wrapper = shallowMount(CitiesPopup, {
        propsData: {
          params:    {},
          value:     false,
          isLoading: false,
          cityName:  '',
        },
      });

      await wrapper.setData({
        autoCheckLoading: true,
      });
      await wrapper.vm.autoCheck();

      expect(detectCityMock).not.toHaveBeenCalled();
    });

    it('should not call Sunlight.modules.regions if isAutoCheckDone', async () => {
      expect.hasAssertions();

      const detectCityMock = jest.fn();

      jest.spyOn(CitiesPopup, 'created').mockReturnValue({});

      window.Sunlight = {
        modules: {
          regions: {
            detectCity: detectCityMock,
          },
        },
      };

      const wrapper = shallowMount(CitiesPopup, {
        propsData: {
          params:    {},
          value:     false,
          isLoading: false,
          cityName:  '',
        },
      });

      await wrapper.setData({
        isAutoCheckDone: true,
      });
      await wrapper.vm.autoCheck();

      expect(detectCityMock).not.toHaveBeenCalled();
    });

    it('should not call Sunlight.modules.regions if isAutoCheckError', async () => {
      expect.hasAssertions();

      const detectCityMock = jest.fn();

      jest.spyOn(CitiesPopup, 'created').mockReturnValue({});

      window.Sunlight = {
        modules: {
          regions: {
            detectCity: detectCityMock,
          },
        },
      };

      const wrapper = shallowMount(CitiesPopup, {
        propsData: {
          params:    {},
          value:     false,
          isLoading: false,
          cityName:  '',
        },
      });

      await wrapper.setData({
        isAutoCheckError: true,
      });
      await wrapper.vm.autoCheck();

      expect(detectCityMock).not.toHaveBeenCalled();
    });

    it(`should call Sunlight.modules.regions if not is loading
        and not autoCheckLoading
        and isAutoCheckDone
        and not isAutoCheckError and set expected attributes on success`, async () => {
      expect.hasAssertions();

      const TEST_CITY_NAME = 'test-city';
      const detectCityMock = jest.fn().mockResolvedValue({
        name: TEST_CITY_NAME,
      });

      jest.spyOn(CitiesPopup, 'created').mockReturnValue({});

      window.Sunlight = {
        modules: {
          regions: {
            detectCity: detectCityMock,
          },
        },
      };

      const wrapper = shallowMount(CitiesPopup, {
        propsData: {
          params:    {},
          value:     false,
          isLoading: false,
          cityName:  '',
        },
      });

      await wrapper.setData({
        isAutoCheckError: false,
        isAutoCheckDone:  false,
        autoCheckLoading: false,
      });
      await wrapper.vm.autoCheck();

      expect(detectCityMock).toHaveBeenCalledWith({ force: true, safe: false });

      expect(wrapper.vm.isAutoCheckDone).toBe(true);
      expect(wrapper.vm.isAutoCheckError).toBe(false);
      expect(wrapper.vm.autoCheckText).toBe(CitiesPopup.autoCheckTexts.doneText);
      expect(wrapper.vm.cityNameInner).toBe(TEST_CITY_NAME);
      expect(wrapper.vm.autoCheckLoading).toBe(false);
    });

    it(`should call Sunlight.modules.regions if not is loading
        and not autoCheckLoading
        and isAutoCheckDone
        and not isAutoCheckError and set expected attributes on fail`, async () => {
      expect.hasAssertions();

      const detectCityMock = jest.fn().mockRejectedValue();

      jest.spyOn(CitiesPopup, 'created').mockReturnValue({});

      window.Sunlight = {
        modules: {
          regions: {
            detectCity: detectCityMock,
          },
        },
      };

      const wrapper = shallowMount(CitiesPopup, {
        propsData: {
          params:    {},
          value:     false,
          isLoading: false,
          cityName:  '',
        },
      });

      await wrapper.setData({
        isAutoCheckError: false,
        isAutoCheckDone:  false,
        autoCheckLoading: false,
      });
      await wrapper.vm.autoCheck();

      expect(detectCityMock).toHaveBeenCalledWith({ force: true, safe: false });

      expect(wrapper.vm.isAutoCheckDone).toBe(false);
      expect(wrapper.vm.isAutoCheckError).toBe(true);
      expect(wrapper.vm.autoCheckText).toBe(CitiesPopup.autoCheckTexts.errorText);
      expect(wrapper.vm.autoCheckLoading).toBe(false);
    });
  });

  describe('testing centering', () => {
    it('should call popup centering if one exists', () => {
      expect.hasAssertions();

      jest.spyOn(CitiesPopup, 'created').mockReturnValue({});
      const wrapper = shallowMount(CitiesPopup, {
        propsData: {
          params:    {},
          value:     false,
          isLoading: false,
          cityName:  '',
        },
      });
      const centeringMock = jest.fn();

      wrapper.vm.popup = {
        centering: centeringMock,
      };

      wrapper.vm.centering();

      expect(centeringMock).toHaveBeenCalledWith();
    });
  });
});

describe('initializing data', () => {
  it('should properly set popular columns, title and all columns based on getRegions data', () => {
    expect.hasAssertions();

    const TEST_CITIES = [...Array(100)].map((_, i) => {
      return {
        is_main: i < 50,
        count:   i,
      };
    });
    const getNounForNumberMock = jest.fn().mockReturnValue('stubbed-noun');

    window.Sunlight = {
      modules: {
        regions: {
          getRegions: jest.fn().mockReturnValue(TEST_CITIES),
        },
      },
      getNounForNumber: getNounForNumberMock,
    };
    const wrapper = shallowMount(CitiesPopup, {
      propsData: {
        params:    {},
        value:     false,
        isLoading: false,
        cityName:  '',
      },
    });

    expect(wrapper.vm.allColumns).toHaveLength(3);
    expect(wrapper.vm.allColumns[0]).toStrictEqual(TEST_CITIES.slice(0, 34));
    expect(wrapper.vm.allColumns[1]).toStrictEqual(TEST_CITIES.slice(34, 68));
    expect(wrapper.vm.allColumns[2]).toStrictEqual(TEST_CITIES.slice(68, 100));

    TEST_CITIES.sort((a, b) => {
      const transformToInt = (region) => {
        return (region.is_main ? 1 : 0) * 1000000 + parseInt(region.count, 10);
      };

      return transformToInt(b) - transformToInt(a);
    });

    expect(wrapper.vm.popularColumns).toHaveLength(3);
    expect(wrapper.vm.popularColumns[0]).toStrictEqual(TEST_CITIES.slice(0, 5));
    expect(wrapper.vm.popularColumns[1]).toStrictEqual(TEST_CITIES.slice(5, 10));
    expect(wrapper.vm.popularColumns[2]).toStrictEqual(TEST_CITIES.slice(10, 15));

    expect(
      wrapper.vm.popularColumns[0].filter((c) => {
        return c.is_main;
      }),
    ).toHaveLength(5);
    expect(
      wrapper.vm.popularColumns[1].filter((c) => {
        return c.is_main;
      }),
    ).toHaveLength(5);
    expect(
      wrapper.vm.popularColumns[2].filter((c) => {
        return c.is_main;
      }),
    ).toHaveLength(5);

    // 4950 - сумма арифметической прогрессии от 0 до 99
    expect(wrapper.vm.title).toStrictEqual('4950 магазинstubbed-noun в 100 городstubbed-noun');
  });
});

describe('testing render', () => {
  it('should call toggleShowAllCities on span.cities-popup__show-all', () => {
    expect.hasAssertions();

    jest.spyOn(CitiesPopup, 'created').mockReturnValue({});
    jest.spyOn(CitiesPopup.methods, 'toggleShowAllCities');
    const wrapper = shallowMount(CitiesPopup, {
      propsData: {
        params:    {},
        value:     false,
        isLoading: false,
        cityName:  '',
      },
    });

    userEvent.click(wrapper.find('.cities-popup__show-all').element);

    expect(CitiesPopup.methods.toggleShowAllCities).toHaveBeenCalledWith();
  });

  it('cities-popup__show-all should have text `Оставить только крупные` if allCitiesAreShowed', async () => {
    expect.hasAssertions();

    jest.spyOn(CitiesPopup, 'created').mockReturnValue({});
    jest.spyOn(CitiesPopup.methods, 'toggleShowAllCities');
    const wrapper = shallowMount(CitiesPopup, {
      propsData: {
        params:    {},
        value:     false,
        isLoading: false,
        cityName:  '',
      },
    });

    await wrapper.setData({
      allCitiesAreShowed: true,
    });

    expect(wrapper.find('.cities-popup__show-all').text()).toStrictEqual('Оставить только крупные');
  });

  it('cities-popup__show-all should have text Показать все города` if not allCitiesAreShowed', async () => {
    expect.hasAssertions();

    jest.spyOn(CitiesPopup, 'created').mockReturnValue({});
    jest.spyOn(CitiesPopup.methods, 'toggleShowAllCities');
    const wrapper = shallowMount(CitiesPopup, {
      propsData: {
        params:    {},
        value:     false,
        isLoading: false,
        cityName:  '',
      },
    });

    await wrapper.setData({
      allCitiesAreShowed: false,
    });

    expect(wrapper.find('.cities-popup__show-all').text()).toStrictEqual('Показать все города');
  });

  it('should call autoCheck on span.cities-popup__auto-check-text', () => {
    expect.hasAssertions();

    jest.spyOn(CitiesPopup, 'created').mockReturnValue({});
    jest.spyOn(CitiesPopup.methods, 'autoCheck');
    const wrapper = shallowMount(CitiesPopup, {
      propsData: {
        params:    {},
        value:     false,
        isLoading: false,
        cityName:  '',
      },
    });

    userEvent.click(wrapper.find('.cities-popup__auto-check-text').element);

    expect(CitiesPopup.methods.autoCheck).toHaveBeenCalledWith();
  });

  it('autoCheck span should have class m-done if isAutoCheckDone', async () => {
    expect.hasAssertions();

    jest.spyOn(CitiesPopup, 'created').mockReturnValue({});
    const wrapper = shallowMount(CitiesPopup, {
      propsData: {
        params:    {},
        value:     false,
        isLoading: false,
        cityName:  '',
      },
    });

    await wrapper.setData({
      isAutoCheckDone: true,
    });

    expect(wrapper.find('.cities-popup__auto-check-text').classes()).toContain('m-done');
  });

  it('autoCheck span should have class m-error if isAutoCheckError', async () => {
    expect.hasAssertions();

    jest.spyOn(CitiesPopup, 'created').mockReturnValue({});
    const wrapper = shallowMount(CitiesPopup, {
      propsData: {
        params:    {},
        value:     false,
        isLoading: false,
        cityName:  '',
      },
    });

    await wrapper.setData({
      isAutoCheckError: true,
    });

    expect(wrapper.find('.cities-popup__auto-check-text').classes()).toContain('m-error');
  });
});
