import { enableAutoDestroy, shallowMount } from '@vue/test-utils';
import CitiesColumns from '../../src/components/CitiesColumns.vue';
import CitiesList from '../../src/components/CitiesList.vue';

enableAutoDestroy(afterEach);

describe('test initial data setting', () => {
  it('should set popularMobileRegions with max quantity of 5', () => {
    expect.hasAssertions();

    const TEST_CITIES = [1, 2, 3, 4, 5, 6];

    const wrapper = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: false,
        popularColumns:     [TEST_CITIES],
        isLoading:          false,
      },
    });

    expect(wrapper.vm.$data.popularMobileRegions).toHaveLength(5);
  });

  it('should set allCitiesAreShowedInner with allCitiesAreShowed value', () => {
    expect.hasAssertions();

    const wrapper1 = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: false,
        isLoading:          false,
      },
    });
    const wrapper2 = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: true,
        isLoading:          false,
      },
    });

    expect(wrapper1.vm.$data.allCitiesAreShowedInner).toStrictEqual(false);
    expect(wrapper2.vm.$data.allCitiesAreShowedInner).toStrictEqual(true);
  });

  it('should set topCitiesAreShowedInner with inverted allCitiesAreShowed value', () => {
    expect.hasAssertions();

    const wrapper1 = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: false,
        isLoading:          false,
      },
    });
    const wrapper2 = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: true,
        isLoading:          false,
      },
    });

    expect(wrapper1.vm.$data.topCitiesAreShowedInner).toStrictEqual(true);
    expect(wrapper2.vm.$data.topCitiesAreShowedInner).toStrictEqual(false);
  });

  it('should set allCitiesAreScrolling: with false', () => {
    expect.hasAssertions();

    const wrapper = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: false,
        isLoading:          false,
      },
    });

    expect(wrapper.vm.$data.allCitiesAreScrolling).toStrictEqual(false);
  });
});

describe('testing CitiesList', () => {
  it('should have cities-popup__list__opened class if allCitiesAreShowed is true', () => {
    expect.hasAssertions();

    const wrapper = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: true,
        isLoading:          false,
      },
    });

    expect(wrapper.classes()).toContain('cities-popup__list__opened');
  });

  it('should not have cities-popup__list__opened class if allCitiesAreShowed is fase', () => {
    expect.hasAssertions();

    const wrapper = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: false,
        isLoading:          false,
      },
    });

    expect(wrapper.classes()).not.toContain('cities-popup__list__opened');
  });

  it('should render exact title in title block', () => {
    expect.hasAssertions();

    const TEST_TITLE = 'test title';
    const wrapper = shallowMount(CitiesList, {
      propsData: {
        title:              TEST_TITLE,
        allCitiesAreShowed: false,
        isLoading:          false,
      },
    });

    expect(wrapper.find('.cities-popup__title').text()).toStrictEqual(TEST_TITLE);
  });
});

describe('testing CitiesColumns initial rendering', () => {
  it('should render mobile columns with expected cities', () => {
    expect.hasAssertions();

    const TEST_CITIES = [1, 2, 3, 4, 5];

    const wrapper = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: false,
        popularColumns:     [TEST_CITIES],
        isLoading:          false,
      },
    });

    expect(wrapper.findAllComponents(CitiesColumns).wrappers[0].props('columns')).toStrictEqual([TEST_CITIES]);
  });

  it('should render desktop popular columns with expected popularColumns', () => {
    expect.hasAssertions();

    const TEST_CITIES = [1, 2, 3, 4, 5, 6, 7, 8];

    const wrapper = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: false,
        popularColumns:     [TEST_CITIES],
        isLoading:          false,
      },
    });

    expect(wrapper.findAllComponents(CitiesColumns).wrappers[1].props('columns')).toStrictEqual([TEST_CITIES]);
  });

  it('should render desktop all columns with expected allColumns', () => {
    expect.hasAssertions();

    const TEST_CITIES = [1, 2, 3, 4, 5, 6, 7, 8];

    const wrapper = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: false,
        allColumns:         [TEST_CITIES],
        isLoading:          false,
      },
    });

    expect(wrapper.findAllComponents(CitiesColumns).wrappers[2].props('columns')).toStrictEqual([TEST_CITIES]);
  });

  it('should call setCity method if CitiesColumns emitted such event', () => {
    expect.hasAssertions();

    jest.spyOn(CitiesList.methods, 'setCity');

    const wrapper = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: false,
        isLoading:          false,
      },
    });
    const TEST_CITY = {
      id:    1,
      name:  `test-city-1`,
      count: 1,
    };

    wrapper.findAllComponents(CitiesColumns).wrappers.forEach((citiesColumnsWrapper) => {
      citiesColumnsWrapper.vm.$emit('set-city', TEST_CITY);

      expect(CitiesList.methods.setCity).toHaveBeenCalledWith(TEST_CITY);

      CitiesList.methods.setCity.mockReset();
    });
  });
});

describe('testing CitiesColumns conditional showing', () => {
  it('mobile columns should be showed if not allCitiesAreShowed', () => {
    expect.hasAssertions();

    const wrapper = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: false,
        isLoading:          false,
      },
    });

    expect(wrapper.findAllComponents(CitiesColumns).wrappers[0].isVisible()).toBe(true);
  });

  it('mobile columns should not be showed if allCitiesAreShowed', () => {
    expect.hasAssertions();

    const wrapper = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: true,
        isLoading:          false,
      },
    });

    expect(wrapper.findAllComponents(CitiesColumns).wrappers[0].isVisible()).toBe(false);
  });

  it('popular columns should be showed if not allCitiesAreShowed', () => {
    expect.hasAssertions();

    const wrapper = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: false,
        isLoading:          false,
      },
    });

    expect(wrapper.findAllComponents(CitiesColumns).wrappers[1].isVisible()).toBe(true);
  });

  it('popular columns should not be showed if allCitiesAreShowed', () => {
    expect.hasAssertions();

    const wrapper = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: true,
        isLoading:          false,
      },
    });

    expect(wrapper.findAllComponents(CitiesColumns).wrappers[1].isVisible()).toBe(false);
  });

  it('all columns should be showed if allCitiesAreShowed', () => {
    expect.hasAssertions();

    const wrapper = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: true,
        isLoading:          false,
      },
    });

    expect(wrapper.findAllComponents(CitiesColumns).wrappers[2].isVisible()).toBe(true);
  });

  it('all columns should not be showed if not allCitiesAreShowed', () => {
    expect.hasAssertions();

    const wrapper = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: false,
        isLoading:          false,
      },
    });

    expect(wrapper.findAllComponents(CitiesColumns).wrappers[2].isVisible()).toBe(false);
  });
});

describe('testing methods', () => {
  it('should emit set-city on setCity method call with passed params', () => {
    expect.hasAssertions();

    const TEST_PARAMS = {
      param1: 1,
      param2: 2,
    };

    const wrapper = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: false,
        isLoading:          false,
      },
    });

    wrapper.vm.setCity(TEST_PARAMS);

    const emittedEvents = wrapper.emitted('set-city');

    expect(emittedEvents).toHaveLength(1);
    expect(emittedEvents[0]).toStrictEqual([TEST_PARAMS]);
  });

  it('should emit animation-has-ended on topCitiesTransitionEnter method call with', () => {
    expect.hasAssertions();

    const wrapper = shallowMount(CitiesList, {
      propsData: {
        title:              '',
        allCitiesAreShowed: false,
        isLoading:          false,
      },
    });

    wrapper.vm.topCitiesTransitionEnter();

    const emittedEvents = wrapper.emitted('animation-has-ended');

    expect(emittedEvents).toHaveLength(1);
  });
});

