import { enableAutoDestroy, shallowMount } from '@vue/test-utils';
import CitiesColumns from '../../src/components/CitiesColumns.vue';
import City from '../../src/components/City.vue';

enableAutoDestroy(afterEach);

describe('testing CityColumns', () => {
  it('should have default type class', () => {
    expect.hasAssertions();

    const TEST_TYPE = 'test-type';
    const wrapper = shallowMount(CitiesColumns, {
      propsData: {
        columns:   [],
        type:      TEST_TYPE,
        isLoading: false,
      },
    });

    expect(wrapper.classes()).toContain(`cities-popup__list-block__${TEST_TYPE}`);
  });

  it('should render first letter as first city`s name if showFirstLetter prop passed', () => {
    expect.hasAssertions();

    const TEST_LETTERS = 'abcdefg';

    TEST_LETTERS.split('').forEach((letter, i) => {
      const wrapper = shallowMount(CitiesColumns, {
        propsData: {
          type:            '',
          showFirstLetter: true,
          isLoading:       false,
          columns:         [
            [
              {
                id:    i,
                name:  `${letter}-started-test-city-${i}`,
                count: i,
              },
            ],
          ],
        },
      });

      const firstLetterEl = wrapper.find('ul > li:first-child');

      expect(firstLetterEl.classes()).toContain('cities-popup__list-column-item__letter');
      expect(firstLetterEl.text()).toStrictEqual(letter);
    });
  });

  it('should render new first letter if previous city started with different letter', () => {
    expect.hasAssertions();

    const TEST_LETTERS = 'abcdefg';
    const TEST_CITIES = TEST_LETTERS.split('').map((letter, i) => {
      return {
        id:    i,
        name:  `${letter}-started-test-city-${i}`,
        count: i,
      };
    });

    const wrapper = shallowMount(CitiesColumns, {
      propsData: {
        type:            '',
        showFirstLetter: true,
        columns:         [TEST_CITIES],
        isLoading:       false,
      },
    });

    const firstLetterElements = wrapper.findAll('ul > li.cities-popup__list-column-item__letter');

    expect(firstLetterElements).toHaveLength(TEST_LETTERS.length);

    firstLetterElements.wrappers.forEach((element, i) => {
      expect(element.text()).toStrictEqual(TEST_LETTERS[i]);
    });
  });

  it('should not render new first letter if previous city started with same letter', () => {
    expect.hasAssertions();

    const TEST_LETTERS = 'aaaaaaaa';
    const TEST_CITIES = TEST_LETTERS.split('').map((letter, i) => {
      return {
        id:    i,
        name:  `${letter}-started-test-city-${i}`,
        count: i,
      };
    });

    const wrapper = shallowMount(CitiesColumns, {
      propsData: {
        type:            '',
        showFirstLetter: true,
        columns:         [TEST_CITIES],
        isLoading:       false,
      },
    });
    const firstLetterElements = wrapper.findAll('ul > li.cities-popup__list-column-item__letter');

    expect(firstLetterElements).toHaveLength(1);
  });

  it('should render ul in quantity of columns', () => {
    expect.hasAssertions();

    [...Array(10)].forEach((el, i) => {
      const wrapper = shallowMount(CitiesColumns, {
        propsData: {
          type:            '',
          showFirstLetter: true,
          columns:         Array(i),
          isLoading:       false,
        },
      });

      expect(wrapper.findAll('ul')).toHaveLength(i);
    });
  });

  it('should render Cities in quantity of columns[]', () => {
    expect.hasAssertions();

    const mockedCityClass = 'city-stabbed';

    [...Array(10)].forEach((_, i) => {
      const wrapper = shallowMount(CitiesColumns, {
        propsData: {
          type:            '',
          showFirstLetter: true,
          isLoading:       false,
          columns:         [
            [...Array(i)].map((__, j) => {
              return {
                name: `city-${j.toString()}`,
                id:   `${i}-${j}`,
              };
            }),
          ],
        },
        stubs: {
          City: { template: `<div class="${mockedCityClass}"></div>` },
        },
      });

      expect(wrapper.findAll(`.${mockedCityClass}`)).toHaveLength(i);
    });
  });

  it('should call setCity on City click with exact params', async () => {
    expect.hasAssertions();

    jest.spyOn(CitiesColumns.methods, 'setCity');

    const TEST_CITY = {
      id:    1,
      name:  `test-city-1`,
      count: 1,
    };
    const wrapper = shallowMount(CitiesColumns, {
      propsData: {
        type:            '',
        showFirstLetter: true,
        columns:         [[TEST_CITY]],
        isLoading:       false,
      },
    });

    const cityWrapper = wrapper.findComponent(City);

    await cityWrapper.trigger('click');

    expect(CitiesColumns.methods.setCity).toHaveBeenCalledWith(TEST_CITY);
  });
});
