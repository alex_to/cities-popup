import { shallowMount } from '@vue/test-utils';
import CitiesSearch from '../../src/components/CitiesSearch.vue';

const autocompleteMock = jest.fn();

// eslint-disable-next-line jest/require-top-level-describe
beforeEach(() => {
  // eslint-disable-next-line jest/prefer-spy-on
  window.$ = jest.fn().mockImplementation(() => {
    return {
      autocomplete: autocompleteMock,
    };
  });

  window.SUNLIGHT = {};
});

describe('testing methods', () => {
  describe('testing setData', () => {
    it('should not emit set-city if fiasId is not defined', () => {
      expect.hasAssertions();

      const wrapper = shallowMount(CitiesSearch, {});

      wrapper.vm.submitForm();

      expect(wrapper.emitted('set-city')).toBeUndefined();
    });

    it('should not emit set-city if currently is loading', async () => {
      expect.hasAssertions();

      const wrapper = shallowMount(CitiesSearch, {});

      await wrapper.setData({
        isLoading: true,
      });
      wrapper.vm.submitForm();

      expect(wrapper.emitted('set-city')).toBeUndefined();
    });

    it('should emit set-city if fiasId is defined and is not currently loading', async () => {
      expect.hasAssertions();

      const wrapper = shallowMount(CitiesSearch, {});
      const TEST_FIAS_ID = 'test-fias-id';

      await wrapper.setData({
        isLoading: false,
        fiasId:    TEST_FIAS_ID,
      });
      wrapper.vm.submitForm();

      expect(wrapper.emitted('set-city')).toHaveLength(1);
    });

    it('should emit set-city with expected data', async () => {
      expect.hasAssertions();

      const wrapper = shallowMount(CitiesSearch, {});
      const TEST_FIAS_ID = 'test-fias-id';
      const TEST_CITY_NAME = 'test-city-name';

      await wrapper.setData({
        isLoading: false,
        fiasId:    TEST_FIAS_ID,
        value:     TEST_CITY_NAME,
      });
      wrapper.vm.submitForm();

      expect(wrapper.emitted('set-city')[0]).toStrictEqual([
        {
          safe:   false,
          name:   TEST_CITY_NAME,
          fiasId: TEST_FIAS_ID,
        },
      ]);
    });
  });

  describe('testing clearInput', () => {
    it('should set value to empty string', async () => {
      expect.hasAssertions();

      const wrapper = shallowMount(CitiesSearch);

      await wrapper.setData({
        value: 'value',
      });

      wrapper.vm.clearInput();

      expect(wrapper.vm.value).toStrictEqual('');
    });
  });
});

describe('testing autocomplete', () => {
  it('should call autocomplete on mount', () => {
    expect.hasAssertions();

    jest.clearAllMocks();

    shallowMount(CitiesSearch);

    expect(autocompleteMock).toHaveBeenCalledTimes(1);
  });
});

describe('testing render', () => {
  it('should not show closing element if input is not filled', () => {
    expect.hasAssertions();

    const wrapper = shallowMount(CitiesSearch);

    expect(wrapper.find('.cities-popup-search__close').isVisible()).toBe(false);
  });

  it('should show closing element if input is filled', async () => {
    expect.hasAssertions();

    const wrapper = shallowMount(CitiesSearch);

    await wrapper.find('input').setValue('test-value');

    expect(wrapper.find('.cities-popup-search__close').isVisible()).toBe(true);
  });

  it('input should not have loading class if not isLoading', () => {
    expect.hasAssertions();

    const wrapper = shallowMount(CitiesSearch);

    expect(wrapper.find('input').classes()).not.toContain('m-loading');
  });

  it('input should have loading class if isLoading', async () => {
    expect.hasAssertions();

    const wrapper = shallowMount(CitiesSearch);

    await wrapper.setData({
      isLoading: true
    })

    expect(wrapper.find('input').classes()).toContain('m-loading');
  });

  it('input should not have valid class if not isInputValid', () => {
    expect.hasAssertions();

    const wrapper = shallowMount(CitiesSearch);

    expect(wrapper.find('input').classes()).not.toContain('m-valid');
  });

  it('input should have valid class if isInputValid', async () => {
    expect.hasAssertions();

    const wrapper = shallowMount(CitiesSearch);

    await wrapper.setData({
      isInputValid: true
    })

    expect(wrapper.find('input').classes()).toContain('m-valid');
  });

  it('should emit clearInput on clear input element', () => {
    expect.hasAssertions();

    jest.spyOn(CitiesSearch.methods, 'clearInput');
    const wrapper = shallowMount(CitiesSearch);

    wrapper.find('.cities-popup-search__close').trigger('click');

    expect(CitiesSearch.methods.clearInput).toHaveBeenCalledTimes(1);
  });
});
