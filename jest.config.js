const path = require('path');
const originalConfig = require('../../jest.config.js');

module.exports = {
  ...originalConfig,
  transform: {
    '\\.js$':  'babel-jest',
    '\\.vue$': 'vue-jest',
  },
  testPathIgnorePatterns: ['/node_modules/'],

  moduleNameMapper: {
    '^@/(.*)$':      `${path.resolve(__dirname)}/src/$1`,
    '~static/(.*)$': `${path.resolve(path.resolve('../../'))}/$1`,
  },
};
