export default (arr, chunksCnt) => {
  const chunks = [];
  let i = 0;
  const len = arr.length;
  const chunkSize = Math.ceil(len / chunksCnt);

  while (i < len) {
    chunks.push(arr.slice(i, i + chunkSize));
    i += chunkSize;
  }
  return chunks;
};
