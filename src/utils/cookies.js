import constants from '../constants.js';

const setPopupShownCookies = (days) => {
  Cookies.set(constants.cityPopupIsAlreadyShownKey, 1, {
    domain:  window.SL_SETTINGS.CUSTOMER_COOKIE_DOMAIN,
    expires: days,
  });
};

export default {
  get hasCookies() {
    return Boolean(Cookies.get(constants.cityPopupIsAlreadyShownKey));
  },

  setPopupShownCookiesForALongTime() {
    setPopupShownCookies(constants.cityPopupIsAlreadyShownDays.long);
  },

  setPopupShownCookiesForAShortTime() {
    setPopupShownCookies(constants.cityPopupIsAlreadyShownDays.short);
  },

  removePopupShownCookies() {
    Cookies.remove(constants.cityPopupIsAlreadyShownKey, {
      domain: window.SL_SETTINGS.CUSTOMER_COOKIE_DOMAIN,
    });
  },
};
