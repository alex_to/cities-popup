export default {
  cityPopupIsAlreadyShownKey:  'city_auto_popup_shown',
  cityPopupIsAlreadyShownDays: {
    long:  7,
    short: 1,
  },
};
