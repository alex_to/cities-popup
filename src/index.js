import './scss/app.scss';
import App from './App.vue';

let popups;
const defaultParams = {
  redirect: true,
};

Sunlight.popupLoader.appendHTML('<div id="region-popups-wrap"></div>');

Sunlight.registerModule('cities-popup', {
  toggle() {
    popups.isOpened = !popups.isOpened;
  },

  open() {
    if (!popups) {
      return;
    }
    popups.isOpened = true;
  },

  close() {
    if (!popups) {
      return;
    }
    popups.isOpened = false;
  },

  init(params = {}) {
    if (popups) {
      return;
    }

    popups = new Vue({
      el: '#region-popups-wrap',
      data() {
        return {
          isOpened: false,
        };
      },
      render(h) {
        return h(App, {
          props: {
            value:  this.isOpened,
            params: { ...defaultParams, ...params },
          },
          on: {
            input: (val) => {
              this.isOpened = val;
            },
          },
        });
      },
    });
  },
});
